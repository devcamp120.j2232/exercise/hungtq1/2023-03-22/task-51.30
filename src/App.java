import java.util.Set;
import java.util.LinkedHashSet;
import java.lang.StringBuilder;
import java.util.*;

public class App {
    public static void main(String[] args) throws Exception {
        //subtask 1 Cho một chuỗi str, xoá các ký tự xuất hiện nhiều hơn một lần trong chuỗi và chỉ giữ lại ký tự đầu tiên
        String str1 = "abcabcabc";
        String result1 = removeDuplicateCharacters(str1);
        System.out.println(result1); // in ra "abc"

        //subtask 2 Cho một xâu kí tự, đếm số lượng các từ trong xâu kí tự đó ( các từ có thể cách nhau bằng nhiều khoảng trắng )
        String str2 = "   Devcamp java   ";
        int count2 = countWords(str2);
        System.out.println(count2); // in ra 5

        //subtask 3 Cho hai xâu kí tự s1, s2 nối xâu kí tự s2 vào sau xâu s1
        String s31 = "abc";
        String s32 = " def";
        String result3 = concatenateStringsSubTask3(s31, s32);
        System.out.println(result3); // in ra "abcdef"

        //subtask 4 Cho hai xâu kí tự s1, s2, kiểm tra xem chuỗi s1 chứa chuỗi s2 không?
        String s41 = "Devcamp java";
        String s42 = "java";
        boolean result4 = containsString(s41, s42);
        System.out.println(result4); // in ra true

        //subtask 5 Hiển thị ký tự thứ k trong string
        String str5 = "Xin chao";
        int k5 = 3;
        char result5 = getCharacterAt(str5, k5);
        System.out.println(result5); // in ra 'c'

        //subtask 6 đếm số lần xuất hiện của một ký tự trong một xâu
        String str6 = "Xin chao";
        char c6 = 'o';
        int count6 = countCharacter(str6, c6);
        System.out.println(count6); // in ra 1

        //subtask 7 Tìm vị trí xuất hiện lần đầu tiên của một ký tự trong một xâu
        String str7 = "Xin chao";
        char c7 = 'o';
        int index7 = findFirstOccurrence(str7, c7);
        System.out.println(index7); // in ra 7

        //subtask 8 Chuyển các ký tự in thường sang in hoa
        String str8 = "Xin chao";
        String result8 = toUpperCase(str8);
        System.out.println(result8); // in ra "XIN CHAO"

        //subtask 9 Chuyển các ký tự in thường sang in hoa và ngược lại
        String str9 = "Xin Chao";
        String result9 = swapCase(str9);
        System.out.println(result9); // in ra "xIN cHAO"

        //subtask 10 Đếm số ký tự in hoa trong một xâu
        String str10 = "Xin Chao";
        int count10 = countUpperCase(str10);
        System.out.println(count10); // in ra 2

        //subtask 11 Hiển thị ra màn hình các ký tự từ A tới Z
        String str11 = "DevCamp123";
        String result11 = getUppercaseCharacters(str11);
        System.out.println(result11); // in ra "DC"

        //subtask 12 Cho một chuỗi str và số nguyên n >= 0. Chia chuỗi str ra làm các phần bằng nhau với n ký tự. Nếu chuỗi không chia được thì xuất ra màn hình “KO”.
        String str12 = "abcdefgh";
        int n12 = 3;
        String[] parts12 = splitString(str12, n12);
        System.out.println(Arrays.toString(parts12)); // in ra "[KO]"

        //subtask 13 Xoá tất cả các ký tự liền kề và giống nhau
        String str13 = "aabbcc";
        String result13 = removeAdjacentDuplicates(str13);
        System.out.println(result13); // in ra "abc"

        //subtask 14 Cho 2 string, gắn chúng lại với nhau, nếu 2 chuỗi có độ dài không bằng nhau thì tiến hành cắt bỏ các ký tự đầu của string dài hơn cho đến khi chúng bằng nhau thì tiến hành gắn lại
        String s141 = "Xin chao";
        String s142 = "the gioi";
        String result14 = concatenateStringsSubTask14(s141, s142);
        System.out.println(result14); // in ra " chaothe gioi"

        //subtask 15 giong subtask 9

        //subtask 16 Kiểm tra xem một chuỗi có xuất hiện số hay không
        String str16 = "Xin chao 123";
        boolean result16 = containsNumber(str16);
        System.out.println(result16);

        //subtask 17 Kiểm tra chuỗi này có phù hợp với yêu cầu hay không Yêu cầu về chuỗi là: Có độ dài không quá 20 ký tự, không được chứa ký tự khoảng trắng, bắt đầu bằng một ký tự chữ viết hoa (A - Z), kết thúc bằng một số (0 - 9). (Sử dụng biểu thức chính quy để ràng buộc định dạng)
        String regex17 = "^[A-Z][^\\s]{0,18}[0-9]$";
        String input17 = "YourInputString";
        boolean isValid17 = input17.matches(regex17);
        System.out.println(isValid17);

    }

    //subtask 1
    public static String removeDuplicateCharacters(String str) {
        Set<Character> set = new LinkedHashSet<>();
        for (char c : str.toCharArray()) {
            set.add(c);
        }
        StringBuilder sb = new StringBuilder();
        for (Character c : set) {
            sb.append(c);
        }
        return sb.toString();
    }
    //subtask 2
    public static int countWords(String str) {
        if (str == null || str.isEmpty()) {
            return 0;
        }
        String[] words = str.trim().split("\\s+");
        return words.length;
    }
    //subtask 3
    public static String concatenateStringsSubTask3(String s1, String s2) {
        return s1 + s2;
    }
    //subtask 4
    public static boolean containsString(String s1, String s2) {
        return s1.contains(s2);
    }
    //subtask 5
    public static char getCharacterAt(String str, int k) {
        if (k < 0 || k >= str.length()) {
            throw new IllegalArgumentException("Index out of range");
        }
        return str.charAt(k);
    }
    //subtask 6
    public static int countCharacter(String str, char c) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == c) {
                count++;
            }
        }
        return count;
    }
    //subtask 7
    public static int findFirstOccurrence(String str, char c) {
        return str.indexOf(c);
    }
    //subtask 8
    public static String toUpperCase(String str) {
        return str.toUpperCase();
    }
    //subtask 9
    public static String swapCase(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (Character.isLowerCase(c)) {
                sb.append(Character.toUpperCase(c));
            } else if (Character.isUpperCase(c)) {
                sb.append(Character.toLowerCase(c));
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }
    //subtask 10
    public static int countUpperCase(String str) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (Character.isUpperCase(str.charAt(i))) {
                count++;
            }
        }
        return count;
    }
    //subtask 11
    public static String getUppercaseCharacters(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c >= 'A' && c <= 'Z') {
                sb.append(c);
            }
        }
        return sb.toString();
    }
    //subtask 12
    public static String[] splitString(String str, int n) {
        if (n <= 0 || str.length() % n != 0) {
            return new String[]{"KO"};
        }
        int numParts = str.length() / n;
        String[] parts = new String[numParts];
        for (int i = 0; i < numParts; i++) {
            parts[i] = str.substring(i * n, (i + 1) * n);
        }
        return parts;
    }
    //subtask 13
    public static String removeAdjacentDuplicates(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (sb.length() == 0 || sb.charAt(sb.length() - 1) != c) {
                sb.append(c);
            }
        }
        return sb.toString();
    }
    //subtask 14
    public static String concatenateStringsSubTask14(String s1, String s2) {
        if (s1.length() > s2.length()) {
            s1 = s1.substring(s1.length() - s2.length());
        } else if (s2.length() > s1.length()) {
            s2 = s2.substring(s2.length() - s1.length());
        }
        return s1 + s2;
    }
    //subtask 16
    public static boolean containsNumber(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }
    }
